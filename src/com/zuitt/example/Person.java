package com.zuitt.example;

// interfaces implementation
// interfaces allow us to have multiple implementations unlike abstract classes it is limited because i can only has one
public class Person implements Actions, Greetings{
    public void sleep(){
        System.out.println("Zzzz...");
    }
    public void run(){
        System.out.println("Running");
    }

    public void morningGreet(){
        System.out.println("Good Morning!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays!");
    }

}
