package com.zuitt.example;

// Child Class of Animal Class
    // "extend" keyword used to inherit the properties and methods if the parent class.
public class Dog extends Animal {

    // properties
    private String breed;

    //constructor
    // we can use this to add default value
    public Dog(){
        // This will inherit the Animal() constructor
        // Also it invokes all Animal properties
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    // getter and setter
    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    // Mehtods
    public void speak(){
        System.out.println("Woof woof");
    }

}
