package com.zuitt.example;

public class Car {

    // Access Modifier
       // - private is only accessible within the class
       // These are used to restrict the scope of  class, constructor, variable, method pr data member.
        // Four Types of Access Modifiers:
            //1. Default - No keyword indicated (accessibility is within the package)
            //2. Private - Properties or method only accessible within the class.
            //3. Protected - Properties and methods are only accessible by the class of the same package and the
                            //subclass present in any package.
            //4. Public - Properties and methods can be accessed from anywhere.

    // Class Creation
    // Four parts of class creation

    // 1. Properties - characteristics of an object.
    private String name;
    private String brand;
    private int yearOfMake;
    // Make a component of a car
    // Driver here is a package from Driver.java
    private Driver driver;

    // 2. Constructors - used to create/instantiate an object:

    // empty constructor - creates object that doesn't have any arguments/parameters.
    // Must be the same with class above
    public Car(){
        // Whenever a new car is created , it will have a driver named Alejandro
        this.driver = new Driver("Alejandro");
    }

    //Parameterized constructor - creates an object with argument/parameters.
    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Alejandro");
    }

    // 3. Getters and Setters - get and set the values of each property of the object.

    // Getters - use to retrieve the value of the instantiated object.
    // data type of the item that is get,  is needed
    public String getName(){
        // we need a return statement
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getYearOfMake(){
        return this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }

    // Setter - use to change the default values of an instantiated object
    // reassign of value
        // can also modify for validation such as conditioning
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYearOfMake(int year_Of_Make){
        if(year_Of_Make < 2022){
            this.yearOfMake = year_Of_Make;
        }
    }
    public void setDriverName(String driver){
        this.driver.setName(driver);
    }

    // 4. Methods - functions of an object it can perform (actions).
    public void drive(){
        System.out.println("The car is running.");
    }

}
