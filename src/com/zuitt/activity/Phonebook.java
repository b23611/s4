package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    private Contact contact;

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook(){}

    public Phonebook(Contact contacts){
        this.contact = new Contact();
    }

    public ArrayList getContacts(ArrayList contacts){
        return this.contacts;
    }

    public void setContacts(Contact contact){
        contacts.add(contact);
        this.contacts = contacts;
    }

    public void checkPhonebook(){
        if(contacts.size() < 1){
            System.out.println("Empty Phonebook");
        } else {
            for(int i = 0; i < contacts.size(); i++){
                System.out.println(contacts.get(i).getName());
                System.out.println("-----------------------------------------");
                System.out.println(contacts.get(i).getName() + " has the following registered number:");
                System.out.println(contacts.get(i).getContactNumber());
                System.out.println(contacts.get(i).getName() + " has the following registered address:");
                System.out.println("my home in " + contacts.get(i).getAddress());
            }
        }
    }
}
