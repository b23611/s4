package com.zuitt.activity;

import com.zuitt.activity.*;


public class Main {
    public static void main(String[] args){
        Phonebook myPhonebook = new Phonebook();
        Contact myContacts = new Contact();
        Contact myContacts2 = new Contact();


        myContacts.setName("John Doe");
        myContacts.setContactNumber("+639152468596");
        myContacts.setAddress("Quezon City");
        myPhonebook.setContacts(myContacts);

        myContacts2.setName("Jane Doe");
        myContacts2.setContactNumber("+639162148573");
        myContacts2.setAddress("Caloocan City");
        myPhonebook.setContacts(myContacts2);

        myPhonebook.checkPhonebook();

    }
}
